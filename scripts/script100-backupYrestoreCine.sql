-- backup de la base de cine
BACKUP DATABASE cine
	TO DISK ='C:\backup\cine.bak'
	WITH INIT, COMPRESSION;

-- restore de la base de cine
RESTORE DATABASE cine
  FROM DISK = 'C:\backup\cine.bak'
  WITH REPLACE
