USE CINE_GABRIEL;
/*
alter table pelilas
  drop constraint FK_GEN_PEL

  */
 



DROP TABLE IF EXISTS  generos;
create table generos(
GEN_ID integer NOT NULL PRIMARY KEY(GEN_ID) IDENTITY,
GEN_DESCRIPCION VARCHAR(50)
);

DROP TABLE IF EXISTS  peliculas;

create table peliculas(
PEL_ID integer NOT NULL PRIMARY KEY(PEL_ID) IDENTITY,
GEN_ID integer NOT NULL,
PEL_DESCRIPCION VARCHAR(50),
constraint FK_GEN_PEL FOREIGN KEY (GEN_ID)  REFERENCES GENEROS(GEN_ID),
);

insert into generos (GEN_DESCRIPCION) values 
	('TERROR'),
	('DRAMA'),
	('ACCION'),
	('DOCUMENTAL'),
	('COMEDIA');


insert into peliculas (GEN_ID, PEL_DESCRIPCION) values
	(5,'La pistola desnuda'),
	(3,	'Gladiador'),
	(2,	'titanic'),
	(5,	'Rambito y Rambon'),
	(1,	'La llamada'),
	(1,	'la Profecia'),
	(5,	'Los ba�eros mas locos del mundo'),
	(1,	'El Esplandor'),
	(3,	'Batman'),
	(3,	'Mision Imposible'),
	(3,	'Encuentro Explosivo'),
	(3,	'Sr y Sra Smith'),
	(3,	'Troya'),
	(1,	'La Isla siniestra');

